import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserManagerRoutingModule } from './user-manager-routing.module';
import { UserManagerComponent } from './user-manager/user-manager.component';
import { UsersListComponent } from './user-manager/users-list/users-list.component';
import { UserDetailsComponent } from './user-manager/user-details/user-details.component';
import { AddEditUserComponent } from './user-manager/add-edit-user/add-edit-user.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [UserManagerComponent, UsersListComponent, UserDetailsComponent, AddEditUserComponent],
  imports: [
    CommonModule,
    UserManagerRoutingModule,
    SharedModule
  ]
})
export class UserManagerModule { }
