import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommunicationService } from 'src/app/shared/services/communication/communication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserDataService } from 'src/app/shared/services/userData/user-data.service';
import apiUrls from './../../../../shared/json/apiUrls.json';
import { Subscription } from 'rxjs';
import { NotificationService } from 'src/app/shared/services/notification/notification.service';

@Component({
  selector: 'app-add-edit-user',
  templateUrl: './add-edit-user.component.html',
  styleUrls: ['./add-edit-user.component.scss']
})
export class AddEditUserComponent implements OnInit, OnDestroy {

  userForm: FormGroup;
  userId: number;
  userData: any;
  isEditMode = false;
  routeSub: Subscription;
  getUserSub: Subscription;
  getUserCommunicationSub: Subscription;
  updateUserSub: Subscription;
  addUserSub: Subscription;

  constructor(private formBuilder: FormBuilder, private communication: CommunicationService,
              private route: ActivatedRoute, private router: Router, private userDataService: UserDataService,
              private notification: NotificationService) {
    this.routeSub = this.route.params.subscribe( params => {
      if (params && params.id) {
        this.userId = params.id;
        this.getUserData();
      }
    });
   }

   ngOnInit() {

    this.userForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(4)]],
      username: ['', [Validators.required]],
      email: [''],
      street: [''],
      suite: [''],
      city: [''],
      zip_code: [''],
      phone: [''],
      website: [''],
      company_name: [''],
      company_phrase: ['']
    });
    this.checkIfDataAvailable();
  }

   checkIfDataAvailable() {
     if (!this.userData && this.router.url === '/users/edit/' + this.userId) {
        this.isEditMode = true;
        const url = apiUrls.USERS + '/' + this.userId;
        this.getUserSub = this.userDataService.getUserData(url).subscribe(result => {
           this.userData = result;
           this.assignUSerFormData();
         }, error => {
          // Here we can handle the error specific to this API call
          // However, as we are using global error hadler, we don't need hadle it here.
          // Here, we can add login such as loader disable or proper messaging on page.
        });
     } else if (this.userData && this.router.url === '/users/edit/' + this.userId) {
       this.isEditMode = true;
       this.assignUSerFormData();
     }
   }

   assignUSerFormData() {
     if (this.userData) {
      this.userForm.patchValue({
        name: this.userData.name ? this.userData.name : null,
        username: this.userData.username ? this.userData.username : null,
        email: this.userData.email ? this.userData.email : null,
        street: (this.userData.address && this.userData.address.street ) ? this.userData.address.street : null,
        suite: (this.userData.address && this.userData.address.suite ) ? this.userData.address.suite : null,
        city: (this.userData.address && this.userData.address.city ) ? this.userData.address.city : null,
        zip_code: (this.userData.address && this.userData.address.zipcode ) ? this.userData.address.zipcode : null,
        phone: this.userData.phone ? this.userData.phone : null,
        website: this.userData.website ? this.userData.website : null,
        company_name: (this.userData.company && this.userData.company.name) ? this.userData.company.name : null,
        company_phrase: (this.userData.company && this.userData.company.catchPhrase) ? this.userData.company.catchPhrase : null,
      });
     }
   }

   addEditUser() {
     const userObj = {
      id: 100,
      name: this.userForm.get('name').value,
      username: this.userForm.get('username').value,
      email: this.userForm.get('email').value,
      address: {
        street: this.userForm.get('street').value,
        suite: this.userForm.get('suite').value,
        city: this.userForm.get('city').value,
        zipcode: this.userForm.get('zip_code').value,
        geo: {
          lat: '-37.3159',
          lng: '81.1496'
        }
      },
      phone: this.userForm.get('phone').value,
      website: this.userForm.get('website').value,
      company: {
        name:  this.userForm.get('company_name').value,
        catchPhrase:  this.userForm.get('company_phrase').value,
        bs:  'harness real-time e-markets'
      }
     };

     if (this.isEditMode) {
       const url = apiUrls.USERS + '/' + this.userId;
       this.updateUserSub = this.userDataService.updateUserData(url, userObj).subscribe(result => {
         this.notification.showSuccess('User updated successfully');
         this.router.navigate( [ 'users'] );
       }, error => {
        // Here we can handle the error specific to this API call
        // However, as we are using global error hadler, we don't need hadle it here.
        // Here, we can add login such as loader disable or proper messaging on page.
      });
     } else {
       const url = apiUrls.USERS;
       this.addUserSub = this.userDataService.addUserData(url, userObj).subscribe(result => {
        this.notification.showSuccess('User added successfully');
        this.router.navigate( [ 'users'] );
       }, error => {
        // Here we can handle the error specific to this API call
        // However, as we are using global error hadler, we don't need hadle it here.
        // Here, we can add login such as loader disable or proper messaging on page.
      });
     }
   }

getUserData() {
    this.getUserCommunicationSub = this.communication.getUserData().subscribe(result => {
      this.userData = result;
    });
  }

  ngOnDestroy(): void {
    // Called once, before the instance is destroyed.
    // Add 'implements OnDestroy' to the class.
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
    if (this.getUserSub) {
      this.getUserSub.unsubscribe();
    }
    if (this.addUserSub) {
      this.addUserSub.unsubscribe();
    }
    if (this.updateUserSub) {
      this.updateUserSub.unsubscribe();
    }
    if (this.getUserCommunicationSub) {
      this.getUserCommunicationSub.unsubscribe();
    }
  }

}
