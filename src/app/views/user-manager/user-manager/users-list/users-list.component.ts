import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  @Input() userData: any;
  @Output() sendSelectedUserId = new EventEmitter<any>();

  constructor(private router: Router) { }

  getUserDetails(userId: number) {
    this.sendSelectedUserId.emit(userId);
  }

  goToAddUser() {
    this.router.navigate( [ 'users/add' ] );
  }

  ngOnInit() {
  }

}
