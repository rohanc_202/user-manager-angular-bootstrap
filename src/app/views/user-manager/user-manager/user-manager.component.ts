import { Component, OnInit, OnDestroy } from '@angular/core';
import {UserDataService} from './../../../shared/services/userData/user-data.service';
import apiUrls from './../../../shared/json/apiUrls.json';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-manager',
  templateUrl: './user-manager.component.html',
  styleUrls: ['./user-manager.component.scss']
})
export class UserManagerComponent implements OnInit, OnDestroy {

  userData: any;
  selectedUser: any;
  getUserSub: Subscription;

  constructor(private userDataService: UserDataService) { }

  ngOnInit() {
    this.getUsersList();
  }

  getUsersList() {
    this.getUserSub = this.userDataService.getUserData(apiUrls.USERS).subscribe(result => {
      this.userData = result;
    }, error => {
      // Here we can handle the error specific to this API call
      // However, as we are using global error hadler, we don't need hadle it here.
      // Here, we can add login such as loader disable or proper messaging on page.
    });
  }

  getSelectedUserDetails($event) {
    this.userData.forEach(element => {
      if (element.id === $event) {
        this.selectedUser = element;
      }
    });
  }

  ngOnDestroy(): void {
    // Called once, before the instance is destroyed.
    // Add 'implements OnDestroy' to the class.
    this.getUserSub.unsubscribe();
  }
}
