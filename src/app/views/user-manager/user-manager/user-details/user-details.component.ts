import { Component, OnInit, Input, TemplateRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { CommunicationService } from 'src/app/shared/services/communication/communication.service';
import { UserDataService } from 'src/app/shared/services/userData/user-data.service';
import apiUrls from './../../../../shared/json/apiUrls.json';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { NotificationService } from 'src/app/shared/services/notification/notification.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit, OnDestroy {

  @Input() selectedUser: any;
  modalRef: BsModalRef;
  deleteUserSub: Subscription;

  constructor(private router: Router, private communication: CommunicationService, private userData: UserDataService,
              private modalService: BsModalService, private notification: NotificationService) { }

  ngOnInit() {
  }

  goToEditUser() {
    this.communication.sendUserData(this.selectedUser);
    this.router.navigate( [ 'users/edit', this.selectedUser.id ] );
  }

  deleteUser() {
    const url = apiUrls.USERS + '/' + this.selectedUser.id;
    this.deleteUserSub = this.userData.deleteUserData(url).subscribe(result =>  {
      this.notification.showSuccess('User deleted successfully');
      this.modalRef.hide();
    }, error => {
      // Here we can handle the error specific to this API call
      // However, as we are using global error hadler, we don't need hadle it here.
      // Here, we can add login such as loader disable or proper messaging on page.
    });
  }

  cancelDeletion() {
    this.modalRef.hide();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  ngOnDestroy(): void {
    // Called once, before the instance is destroyed.
    // Add 'implements OnDestroy' to the class.
    if (this.deleteUserSub) {
      this.deleteUserSub.unsubscribe();
    }
  }
}
