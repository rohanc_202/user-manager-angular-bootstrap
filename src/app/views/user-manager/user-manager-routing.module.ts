import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserManagerComponent } from './user-manager/user-manager.component';
import { UserDetailsComponent } from './user-manager/user-details/user-details.component';
import { AddEditUserComponent } from './user-manager/add-edit-user/add-edit-user.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'users'
  },
  {
    path: 'users',
    component: UserManagerComponent
  },
  {
    path: 'users/add',
    component: AddEditUserComponent
  },
  {
    path: 'users/edit/:id',
    component: AddEditUserComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserManagerRoutingModule { }
