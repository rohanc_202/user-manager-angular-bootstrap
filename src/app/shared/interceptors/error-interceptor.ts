import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpRequest, HttpHandler, HttpInterceptor, HttpErrorResponse} from '@angular/common/http';
import { Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import { ErrorService } from '../services/error/error.service';
import { NotificationService } from '../services/notification/notification.service';

@Injectable()
export class ServerErrorInterceptor implements HttpInterceptor {

  constructor(private injector: Injector) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).pipe(
      catchError((error: any ) => {
        if (error instanceof HttpErrorResponse && error.status === 401) {
          return throwError('Unauthorized user');
        } else if ( error.status === 500) {
          return throwError('Internal server error');
        } else if (error.status === 400) {
          this.showNotification(error);
          return throwError('Something went wrong');
        } else {
          this.showNotification('Something went wrong');
          return throwError('Something went wrong');
        }
      })
    );
  }

  showNotification(error) {
    const errorService = this.injector.get(ErrorService);
    const notifier = this.injector.get(NotificationService);
    let message;

    if (error instanceof HttpErrorResponse) {
            message = errorService.getServerMessage(error.error);
            notifier.showError(message);
        } else {
            message = errorService.getClientMessage(error);
            notifier.showError('Something went wrong');
        }
    }
}
