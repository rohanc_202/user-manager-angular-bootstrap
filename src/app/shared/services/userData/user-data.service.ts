import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  constructor(private http: HttpClient) { }
  baseUrl = environment.url;

  getUserData(url: any) {
    const apiUrl = this.baseUrl + url;
    return this.http.get<any>( apiUrl);
  }

  addUserData(url: any, payload: any) {
    const apiUrl = this.baseUrl + url;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json'
      })
    };
    return this.http.post<any>( apiUrl, payload, httpOptions);
  }

  updateUserData(url: any, payload: any) {
    const apiUrl = this.baseUrl + url;
    return this.http.put<any>( apiUrl, payload);
  }

  deleteUserData(url: any) {
    const apiUrl = this.baseUrl + url;
    return this.http.delete<any>( apiUrl);
  }

  getUserDataWithParams(url: any, apiParams: any) {
    const apiUrl = this.baseUrl + url;
    const httpOptions = {
      params: apiParams
    };
    return this.http.get<any>( apiUrl, httpOptions );
  }
}
