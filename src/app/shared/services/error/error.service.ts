import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor() { }

  getClientMessage(error: Error): string {
    // if (navigator.onLine) {
    //   return 'No Internet Connection';
    // }
    return error.message ? error.message : 'Something went wrong';
  }

  getClientStack(error: Error): string {
    return error.stack;
  }
  getServerMessage(error): string {
    if (!navigator.onLine) {
      return 'No Internet Connection';
    }
    if (!error.message) {
      return error.response;
    } else {
      return error.message;
    }
  }

  getServerStack(error: HttpErrorResponse): string {
    // Handle stack trace
    return 'stack';
  }
}
