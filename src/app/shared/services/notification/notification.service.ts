import { Injectable } from '@angular/core';
import Noty from 'noty';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor() { }

  showSuccess(message: string): void {
    new Noty ({
      text: message,
      type: 'success',
      killer: true,
      timeout : 3000
    }).show();
  }

  showError(message: string): void {
    new Noty ({
      text: message,
      type: 'error',
      killer: true,
      timeout : 3000
    }).show();
  }
}
