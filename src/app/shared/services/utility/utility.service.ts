import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor() { }

  sortByAlphabeticalOrder(array: any, property: any) {
    if  (property ) {
      return array.sort( (a, b) => a[property] > b[property] ? 1 : -1);
    } else {
      return array.sort( (a, b) => a > b ? 1 : -1);
    }
  }
}
