import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppContainerComponent } from './container/app-container/app-container.component';


const routes: Routes = [
  {
    path: '',
    component: AppContainerComponent,
    children: [
        {
            path: '',
            loadChildren: () => import('./views/user-manager/user-manager.module').then(m => m.UserManagerModule)
        }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
